import os
import requests

from celery import Celery

app = Celery(
    'downloader',
    backend='rpc://',
    broker='pyamqp://guest@localhost//'
)


def request(url, method='GET', data=None, params=None):
    try:
        if method == 'GET':
            res = requests.get(url, params=params)
        elif method == 'POST':
            res = requests.post(url, data=data)
        else:
            res = requests.request(method, url)
        res.raise_for_status()
    except requests.HTTPError:
        print('API request failed! status_Code:', res.status_code)
        return False, None
    else:
        return res.ok, res


@app.task
def download_file(url):
    file_name = url.replace('/', '_')
    if not os.path.exists("media/{}".format(file_name)):
        print('Download for url: %s started!' % url)
        response = requests.get(url)
        open("media/{}".format(file_name), 'wb').write(response.content)
        print('Download completed!')
    else:
        print('Download failed! %s is already exists!' % url)
