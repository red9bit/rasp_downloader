from .requests import request

TOKEN = '1004487092:AAHL2MohNreOf4ntqrah4tYfj8AvqlZkMsA'
TELEGRAM_BASE_URI = 'https://api.telegram.org/bot{token}/{method_name}'


def get_url(method_name):
    return TELEGRAM_BASE_URI.format(
        token=TOKEN,
        method_name=method_name
    )


def get_me():
    is_ok, _ = request(get_url('getMe'))
    return is_ok


def get_updates(update_id):
    params = {}
    if update_id:
        params.update(offset=update_id)

    is_ok, res = request(get_url('getUpdates'), params=params)
    results = res.json()['result']
    if is_ok:
        return results
    return []


def send_message(chat_id, text):
    data = {
        'chat_id': chat_id,
        'text': text
    }
    is_ok, res = request(get_url('sendMessage'), method='POST', data=data)

    if is_ok:
        print('Message has been sent to the user')
    else:
        print('Could not send the message to user! something went wrong!')
    print('-' * 30)
