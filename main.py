from time import sleep
from datetime import datetime

from rasp_downloader.requests import download_file
from rasp_downloader import telegram_bot
from rasp_downloader.utils import url_pattern_checker


def main():
    start_time = datetime.now()
    print('------------------------BOT STARTED at %s------------------------' % start_time.isoformat())
    # check connection to telegram bot
    if not telegram_bot.get_me():
        print('Could not connect to telegram bot')
        return

    global last_update_id
    last_update_id = 0

    while True:
        try:
            results = telegram_bot.get_updates(last_update_id + 1)
            messages_count = len(results)
            if messages_count:
                print('Number of received messages:', messages_count)
            else:
                print('There is no new message!')

            for result in results:
                last_update_id = result['update_id']
                message = result.get('message', result.get('edited_message'))
                text = message.get('text')
                first_name = message.get('from', {}).get('first_name')
                username = message.get('from', {}).get('username')
                chat_id = message['chat']['id']

                print('-' * 30)
                print('username:', username)
                print('first name:', first_name)
                print('message:', text)
                print('update_id', last_update_id)
                print('-' * 30)

                if text == '/start':
                    msg = '''Hello and welcome to Raspberry Pi downloader.\nSend me the URL to download! 😉'''
                else:
                    res = url_pattern_checker(text)
                    if res:
                        download_file.delay(text)
                        msg = 'Download has been started.\nURL: %s' % text
                    else:
                        msg = 'Invalid URL. Please send a valid URL.'
                telegram_bot.send_message(chat_id, msg)
            sleep(2)
        except KeyboardInterrupt:
            end_time = datetime.now()
            print('------------------------BOT STOPPED at %s------------------------' % end_time.isoformat())
            print('Total time elapsed:', end_time - start_time)
            return


if __name__ == '__main__':
    main()
